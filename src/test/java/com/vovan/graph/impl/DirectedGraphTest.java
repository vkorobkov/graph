package com.vovan.graph.impl;

import com.vovan.graph.Graph;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DirectedGraphTest {

    private Graph<String> graph;

    @Before
    public void setup() {
        graph = new DirectedGraph<>();
    }

    @Test
    public void shouldFindTwoVertexedPath() {
        graph.addEdge("a", "b");

        List<String> path = graph.getPath("a", "b");

        assertEquals("a", path.get(0));
        assertEquals("b", path.get(1));
    }

    @Test
    public void shouldNotFindPathInBackDirection() {
        graph.addEdge("a", "b");

        List<String> path = graph.getPath("b", "a");

        assertTrue(path.isEmpty());
    }

    @Test
    public void shouldFindPathOfConnectedVertexes() {
        graph.addEdge("a", "b");
        graph.addEdge("b", "c");
        graph.addEdge("c", "d");
        graph.addEdge("a", "z");
        graph.addEdge("z", "e");

        List<String> path = graph.getPath("a", "d");

        assertEquals("a", path.get(0));
        assertEquals("b", path.get(1));
        assertEquals("c", path.get(2));
        assertEquals("d", path.get(3));
    }

    @Test
    public void shouldNotFindPathOfDisconnectedVertexes() {
        graph.addEdge("a", "b");
        graph.addEdge("b", "c");
        graph.addEdge("a", "z");
        graph.addEdge("z", "e");

        List<String> path = graph.getPath("b", "e");

        assertTrue(path.isEmpty());
    }
}