package com.vovan.graph;

import java.util.List;

public interface Graph<Vertex> {

    void addVertex(Vertex vertex);

    void addEdge(Vertex fromVertex, Vertex toVertex);

    List<Vertex> getPath(Vertex vertex1, Vertex vertex2);
}
