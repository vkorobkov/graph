package com.vovan.graph.impl;

public class DirectedGraph<Vertex> extends AbstractGraph<Vertex>  {
    @Override
    protected void addEdgeConnections(Vertex from, Vertex to) {
        addVertexConnection(from, to);
    }
}
