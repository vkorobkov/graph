package com.vovan.graph.impl;

import com.vovan.graph.Graph;

import java.util.*;

public abstract class AbstractGraph<Vertex> implements Graph<Vertex> {

    private final Map<Vertex, VertexConnections<Vertex>> vertexes = new HashMap<>();

    @Override
    public void addVertex(Vertex vertex) {
        if (!vertexes.containsKey(vertex)) {
            vertexes.put(vertex, new VertexConnections<>());
        }
    }

    @Override
    public void addEdge(Vertex fromVertex, Vertex toVertex) {
        addVertex(fromVertex);
        addVertex(toVertex);

        addEdgeConnections(fromVertex, toVertex);
    }

    @Override
    public List<Vertex> getPath(Vertex vertex1, Vertex vertex2) {
        return deepSearch(new HashSet<>(), vertex1, vertex2);
    }

    private List<Vertex> deepSearch(Set<Vertex> scanned, Vertex toScan, Vertex toFind) {
        if (!scanned.add(toScan)) {
            return Collections.EMPTY_LIST;
        }

        if (toFind == toScan) {
            return Collections.singletonList(toScan);
        }

        for (Vertex connectedVertex: getConnectedVertexes(toScan)) {
            List<Vertex> subPath = deepSearch(scanned, connectedVertex, toFind);
            if (!subPath.isEmpty()) {
                List<Vertex>path = new LinkedList<>();
                path.add(toScan);
                path.addAll(subPath);
                return path;
            }
        }

        return Collections.EMPTY_LIST;
    }

    protected abstract void addEdgeConnections(Vertex from, Vertex to);

    void addVertexConnection(Vertex from, Vertex to) {
        vertexes.get(from).addConnection(to);
    }

    private Set<Vertex> getConnectedVertexes(Vertex vertex) {
        return vertexes.get(vertex).connections;
    }

    static class VertexConnections<Vertex> {

        private final Set<Vertex> connections = new HashSet<>();

        public void addConnection(Vertex connectedTo) {
            connections.add(connectedTo);
        }
    }
}
